<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Posts;
/* @var $this yii\web\View */
/* @var $searchModel app\models\PostsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Posts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="posts-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Posts', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'id',
            'title',
            'body:ntext',
            ['attribute' => 'author' , 'value' => 'user.name'],
            ['attribute' => 'category' , 'value' => 'user1.category_name'],
            ['attribute' => 'status' , 'value' => 'user2.status_name'],
           
            //'created_at',
            //'updated_at',
            //'created_by',
            //'updated_by',

            ['attribute' => 'created_by' , 'value' => 'createdBy.username'],
            ['attribute' => 'updated_by' , 'value' => 'updatedBy.username'],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
