<?php

namespace app\models;
use app\models\User;
use app\models\Category;
use app\models\Status;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "posts".
 *
 * @property int $id
 * @property string $title
 * @property string $body
 * @property int $author
 * @property int $status
 * @property string $category
 * @property string $created_at
 * @property string $updated_at
 * @property int $created_by
 * @property int $updated_by
 */
class Posts extends \yii\db\ActiveRecord
{

    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::className(),
              
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'posts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['body'], 'string'],
            [['author', 'status', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['title', 'category'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'body' => 'Body',
            'author' => 'Author',
            'status' => 'Status',
            'category' => 'Category',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    public function beforeSave($insert)
    {//מוודא שרק מי שיש לו הרשאה של אדיטור ומעלה יוכל לפרסם פוסט שהוא פאבליש
        //preventing from users without editor or author permission
        //change from draft to publish
        if (parent::beforeSave($insert)) {
            if (\Yii::$app->user->can('editor')) {
                return true;            
            } else {
                if($this->status == 1){
                    return true;  
                } else {
                    return false;     
                }
            }
        }
        return false;
    }      


    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'author']);
    }
    public function getUser1()
    {
        return $this->hasOne(Category::className(), ['id' => 'category']);
    }
    public function getUser2()
    {
        return $this->hasOne(Status::className(), ['id' => 'status']);
    }
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }
}
